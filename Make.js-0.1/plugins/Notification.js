if(!Make.alert){
  Make.alert = function(content, confirmCallback){
    var object = Make.object("Notification");
    object.addButton("Got it", confirmCallback, "enter");
    object.content(content);
    return object;
  };
}

if(!Make.confirm){
  Make.confirm = function(content, confirmCallback, cancelCallback){
    var object = Make.object("Notification");
    object.addButton("Cancel", cancelCallback, "esc");
    object.addButton("Accept", confirmCallback, "enter");
    object.content(content);
    return object;
  };
}

Make.Notification.object = (function(){

  var Notification = function(){
    Make.plugin("ButtonArea");
  };

  Make.extension(Notification.prototype, {

    make: function(){
      var self = this;

      self.container = $("<div />", {
        id: "notification",
        html: [
          $("<div />", {
            id: "content",
            html: [
              (self.contentArea = $("<div />", {
                id: "content-area"
              }))
            ]
          })
        ]
      }).appendTo($(document.body));
      (self.buttonArea = $("<div />", {
        class: "button-area"
      })).data("parent", self).insertAfter(self.contentArea);

    },

    addButton: function(name, callback, keys){
      var self = this;

      button = $("<div />", {
        id: name.dasherize(),
        label: name
      }).data("callback", function(){
        if(typeof callback === "function"){
          callback.call(this);
        }
        this.container.hide();
      }).data("keys", keys);
      self.buttonArea.append(button);
    },

    content: function(content){
      this.contentArea.html(content);
    }

  });

  return Notification;

})();