Make.Version = {};
Make.Version.object = (function(){

  var Version = function(image){
    var self = this;

    self.fileType = /data:image\/(.+);base64,/.exec(image.src)[1];

    self.crops = {};

    self.censors = [];

    self.censorDeleteButtons = [];

    self.resolution = {
      w: image.naturalWidth,
      h: image.naturalHeight
    };

    self.types = {};

    Make.Uploader.options.CROP_TYPES.each(function(dimension, sizes){
      sizes = sizes.filter(function(size){
        return self.cropDimensions(size).w * Make.Uploader.options.MINIMUM_DPI <= self.resolution.w && self.cropDimensions(size).h * Make.Uploader.options.MINIMUM_DPI <= self.resolution.h;
      });
      if(sizes.length > 0){
        self.types[dimension] = sizes;
        self.crops[dimension] = undefined;
      }
    });

    self.nameChooser = $("<div />", {
      class: "chooser",
      label: "Name:",
      type: "textbox"
    });

    self.ratingChooser = $("<div />", {
      class: "chooser",
      label: "Rating:",
      type: "radio",
      choices: "SFW, NSFW"
    });

    self.thumbnail = $("<div />", {
      class: "version",
      html: [
        (self.image = $(image)),
        (self.propertiesButton = $("<div />", {
          class: "properties-button",
          text: "Properties"
        }).on("click.Version", function(){
          if(self.propertiesBox){
            self.propertiesBox.container.show();
          }else{
            self.propertiesBox = Make.confirm([self.nameChooser, self.ratingChooser], function(){
              this.container.find(".chooser").each(function(index, chooser){
                $(chooser).trigger("accept.Chooser");
              });
            }, function(){
              this.container.find(".chooser").each(function(index, chooser){
                $(chooser).trigger("revert.Chooser");
              });
            });
          }
        }))
      ]
    });

  };

  Make.extension(Version.prototype, {

    cropDimensions: function(size){
      var self = this;

      size = Make.default(size, self.selected);
      return {
        w: parseInt(/(\d+)x/.exec(size)[1]),
        h: parseInt(/x(\d+)/.exec(size)[1])
      };
    },

    reverseCropLabel: function(){
      var self = this;

      var newLabel = self.cropDimensions().h + "x" + self.cropDimensions().w;
      self.crops[newLabel] = self.crops[self.selected];
      delete self.crops[self.selected];
      self.selected = newLabel;
      return self.selected;
    },

    ratio: function(){
      var self = this;

      return self.cropDimensions().w / self.cropDimensions().h;
    },

    uploadData: function(index){
      var self = this;

      return {
        image: self.image.attr("src").replace(/.*,/, ""),
        name: Make.default(self.nameChooser.attr("value"), "Version " + index),
        rating: self.ratingChooser.attr("value"),
        filetype: self.fileType,
        resolution: self.resolution,
        types: self.types,
        crops: self.crops,
        censors: self.censors
      };
    }

  });

  return Version;

})();

Make.Uploader.object = (function(){

  var Uploader = new Function();

  Make.extension(Uploader.prototype, {

    make: function(){
      var self = this;

      Make.plugin("TabArea");
      Make.plugin("ButtonArea");
      Make.plugin("Selector");
      Make.plugin("DialogueBox");
      Make.plugin("Notification");
      Make.plugin("Chooser");

      self.uploadLimit = parseInt(self.container.attr("max-upload-size"));

      self.postUrl = self.container.attr("post-url");

      self.versions = [];

      self.versionFunction = self.firstLoad;

      self.fileField = $("<input />", {
        type: "file",
        multiple: "true"
      }).on("change.Uploader", function(event, file){
        if(file instanceof File){
            self.loading(true);
            if(Make.Uploader.options.ACCEPTED_FILETYPES.contains(file.type)){
              if(file.size > self.uploadLimit){
                Make.alert(Make.Uploader.options.OVER_UPLOAD_LIMIT_MESSAGE(file.size, self.uploadLimit), function(){
                  self.loading(false);
                });
                return;
              }
              self.reader.readAsDataURL(file);
            }else{
              if(/image/.test(file.type)){
                Make.alert(Make.Uploader.options.FORBIDDEN_FILETYPE_MESSAGE(), function(){
                  self.loading(false);
                });
              }else{
                Make.alert(Make.Uploader.options.FAILED_FILE_LOAD_MESSAGE(), function(){
                  self.loading(false);
                });
              }
            }
        }else{
          self.files = Array.from(self.fileField.get(0).files);
          self.container.attr("loading", "(1)");
          self.fileField.trigger("change.Uploader", self.files.splice(0, 1));
        }
      });

      self.container.on("contextmenu.Uploader", function(){
        return false;
      }).on("click.Uploader", function(){
        self.fileField.show();
        self.fileField.trigger("click");
        self.fileField.hide();
      }).before(self.fileField.hide());

      self.reader = new FileReader();

      self.reader.onload = function(){
        var image = new Image();
        image.onerror = function(){
          Make.alert(Make.Uploader.options.FAILED_FILE_LOAD_MESSAGE(), function(){
            self.loading(false);
          });
        };
        image.onload = function(){
          if(image.naturalWidth + image.naturalHeight === 0){
            Make.alert(Make.Uploader.options.FAILED_FILE_LOAD_MESSAGE(), function(){
              self.loading(false);
            });
          }
          var currentImage = self.image.get(0), version = new Make.Version.object(image), jpegWarning = function(){
            if(version.fileType === "jpeg"){
              Make.confirm(Make.Uploader.options.JPEG_MESSAGE(), function(){
                conditionalLoad();
              }, function(){
                self.loading(false);
              });
            }else{
              conditionalLoad();
            }
          }, conditionalLoad = function(){
            var sizes = [];
            version.types.each(function(dimension, types){
              sizes = sizes.concat(types);
            });
            switch(sizes.length){
              case 0:
                Make.alert(Make.Uploader.options.NO_SIZES_MESSAGE(), function(){
                  self.loading(false);
                });
                break;
              case Make.Uploader.options.PRINT_SIZES:
                Make.alert(Make.Uploader.options.ALL_SIZES_MESSAGE(), function(){
                  self.load(version);
                });
                break;
              default:
                Make.confirm(Make.Uploader.options.SOME_SIZES_MESSAGE(sizes), function(){
                  self.load(version);
                }, function(){
                  self.loading(false);
                });
            }
          };
          switch(true){
            case(!currentImage.src):
              jpegWarning();
              break;
            case(currentImage.naturalWidth !== image.naturalWidth || currentImage.naturalHeight !== image.naturalHeight):
              Make.confirm(Make.Uploader.options.DIFFERENT_IMAGE_MESSAGE(), function(){
                jpegWarning();
              }, function(){
                self.loading(false);
              });
              break;
            default:
              self.load(version);
          }
        };
        image.src = self.reader.result;
      };

      self.image = $("<img />").on("click.Uploader", function(){
        self.fileField.show();
        self.fileField.trigger("click.Uploader");
        self.fileField.hide();
      }).on("load.Uploader", function(){
        self.resize.call(self);
        self.tabArea.children(".selected").trigger("click.TabArea");
      });

      self.canvas = $("<canvas />");

      self.context = self.canvas.get(0).getContext("2d");

      self.backCanvas = $("<canvas />");

      self.backContext = self.backCanvas.get(0).getContext("2d");

      self.titleChooser = $("<div />", {
        class: "chooser",
        label: "Submission title:",
        type: "textbox"
      });

      self.originalChooser = $("<div />", {
        class: "chooser",
        label: "Layered original (optional):",
        type: "file"
      });

      self.signatureChooser = $("<div />", {
        class: "chooser",
        label: "Signature file (optional):",
        type: "file"
      });

      self.tabArea = $("<div />", {
        class: "tab-area",
        pos: "n",
        orientation: "right",
        html: [
          $("<div />", {
            id: "post",
            text: "Post"
          }).data("callback", function(){
            var self = this;

            if(self.uploadDialogue){
              self.uploadDialogue.container.show();
            }else{
              self.uploadDialogue = Make.dialogueBox([
                {
                  content: [self.titleChooser, self.originalChooser, self.signatureChooser],
                  buttons: {
                    Next: {
                      keys: "right, enter",
                      callback: function(){
                        self.uploadDialogue.container.trigger("next.DialogueBox");
                      }
                    },
                    Cancel: {
                      keys: "esc",
                      callback: function(){
                        this.container.hide();
                        self.tabArea.trigger("prev.TabArea");
                      }
                    }
                  }
                },
                {
                  content: [
                    $("<div />", {
                      id: "log",
                      html: $("<span />", {
                        text: "Check that you are finished then tap the Upload button to post your submission."
                      })
                    }),
                    $("<div />", {
                      id: "progress",
                      html: [
                        $("<span />", {
                          id: "amount"
                        }),
                        $("<span />", {
                          id: "percent"
                        }),
                        $("<progress />", {
                          value: 0,
                          max: 100
                        })
                      ]
                    })
                  ],
                  buttons: {
                    Upload: {
                      callback: function(){
                        var log = this.container.find("#log > span"),
                            amount = this.container.find("#amount"),
                            percent = this.container.find("#percent"),
                            progress = this.container.find("progress"),
                            buttons = this.buttonArea.children().addClass("disabled"),
                            post = function(index){
                              log.text("Uploading image " + (index + 1) + " / " + self.versions.length);
                              $.ajax({
                                url: self.postUrl,
                                type: "POST",
                                data: {
                                  title: Make.default(self.titleChooser.attr("value"), "Untitled"),
                                  version: JSON.stringify(self.versions[index].uploadData(index))
                                },
                                xhr: function(){
                                  var request = new XMLHttpRequest();
                                  request.upload.addEventListener("progress", function(event){
                                    if(event.lengthComputable){
                                      var percentage = (event.loaded / event.total * 100).toString();
                                      amount.text(event.loaded + " / " + event.total + " bytes");
                                      percent.text(percentage + "%");
                                      progress.attr("value", percentage);
                                    }
                                  }, false);
                                  return request;
                                },
                                success: function(){
                                  if(index < self.versions.length - 1){
                                    post(index + 1);
                                  }else{
                                    self.uploadDialogue.container.trigger("next.DialogueBox");
                                  }
                                },
                                error: function(){
                                  log.addClass("error").text(Make.Uploader.options.SERVER_ERROR_MESSAGE());
                                  buttons.removeClass("disabled");
                                }
                              });
                            };
                        post(0);
                      }
                    },
                    Back: {
                      keys: "left, backspace",
                      callback: function(){
                        self.uploadDialogue.container.trigger("prev.DialogueBox");
                      }
                    },
                    Cancel: {
                      keys: "esc",
                      callback: function(){
                        this.container.hide();
                        self.tabArea.trigger("prev.TabArea");
                      }
                    }
                  }
                },
                {
                  content: $("<div />", {
                    id: "log",
                    html: $("<span />", {
                      text: "Success!"
                    })
                  }),
                  buttons: {
                    "View Submission": {
                      callback: function(){
                        $(location).attr("href", self.container.attr("redirect"));
                      }
                    },
                    "Refresh Page": {
                      callback: function(){
                        location.reload();
                      }
                    }
                  }
                }
              ]);
              self.container.append(self.uploadDialogue.container);
            }
          }),
          $("<div />", {
            id: "drop-version",
            track: "false",
            text: "Drop Version"
          }).data("callback", function(){
            var self = this;

            if(self.versions.length === 1){
              Make.alert(Make.Uploader.options.CANNOT_DROP_IMAGE_MESSAGE());
            }else{
              Make.confirm(Make.Uploader.options.CONFIRM_IMAGE_DROP_MESSAGE(), function(){
                var selected = self.versionArea.children(".selected");
                self.versions.splice(selected.index(), 1);
                var fallback = selected.is(":first-child")? selected.next() : selected.prev();
                selected.remove();
                fallback.trigger("click.Selector");
              });
            }
            self.tabArea.trigger("prev.TabArea");
          }),
          $("<div />", {
            id: "add-version",
            track: "false",
            text: "Add Version"
          }).data("callback", function(){
            var self = this;

            self.versionFunction = self.newVersion;
            self.fileField.show();
            self.fileField.trigger("click");
            self.fileField.hide();
          }),
          $("<div />", {
            id: "censor",
            text: "Censor"
          }).data("callback", function(){
            var self = this;

            self.container.append(self.backCanvas, self.canvas);
            self.resetCensorCanvas();
            self.drawCurrentCensors();
            self.container.append(self.currentVersion.censorDeleteButtons);

            self.canvas.on("mousedown.Uploader", function(event){
              var mousedown = event;
              self.canvas.on("mousemove.Uploader", function(event){
                self.resetCensorCanvas();
                self.drawCensorBox({
                  x: Math.min(mousedown.offsetX, event.offsetX),
                  y: Math.min(mousedown.offsetY, event.offsetY),
                  w: Math.abs(event.offsetX - mousedown.offsetX),
                  h: Math.abs(event.offsetY - mousedown.offsetY)
                });
              self.drawCurrentCensors();
              });
              self.canvas.one("mouseup.Uploader", function(event, box){
                self.canvas.off("mousemove.Uploader");
                self.resetCensorCanvas();
                box = Make.defaults(box, {
                  x: event.offsetX,
                  y: event.offsetY
                });
                box = {
                  x: Math.min(mousedown.offsetX, box.x),
                  y: Math.min(mousedown.offsetY, box.y),
                  w: Math.abs(box.x - mousedown.offsetX),
                  h: Math.abs(box.y - mousedown.offsetY)
                };
                self.drawCensorBox(box);
                self.currentVersion.censors.push({
                  x: box.x / self.currentVersion.ratios.w,
                  y: box.y / self.currentVersion.ratios.h,
                  w: box.w / self.currentVersion.ratios.w,
                  h: box.h / self.currentVersion.ratios.h
                });
                self.currentVersion.censorDeleteButtons.push($("<div />", {
                  class: "censor-delete-button",
                  css: {
                    left: box.x + box.w + self.canvas.offset().left - 15,
                    top: box.y - 15
                  }
                }).on("click.Uploader", function(){
                  var button = $(this);
                  self.context.clearRect(box.x, box.y, box.w, box.h);
                  self.currentVersion.censors.splice(self.currentVersion.censors.indexOf(box), 1);
                  button.remove();
                  self.currentVersion.censorDeleteButtons.splice(self.currentVersion.censorDeleteButtons.indexOf(button), 1);
                }).appendTo(self.container));
                self.drawCurrentCensors();
              });
            });
          }),
          $("<div />", {
            id: "crop",
            text: "Crop"
          }).data("callback", function(){
            var self = this;

            self.container.attr("crop", "Use buttons on right to custom crop for prints of specified dimensions (optional)");
            self.container.append(self.canvas);
            self.cropButtonArea.show();
            self.cropButtonArea.children().removeClass("confirm");
            self.canvas.on("mousemove.Uploader.cursor", function(event){
              if(self.currentVersion.intermediateCrop && event.offsetX > self.currentVersion.intermediateCrop.x && event.offsetX < self.currentVersion.intermediateCrop.x + self.currentVersion.intermediateCrop.w && event.offsetY > self.currentVersion.intermediateCrop.y && event.offsetY < self.currentVersion.intermediateCrop.y + self.currentVersion.intermediateCrop.h){
                self.canvas.addClass("grabbable");
              }else{
                self.canvas.removeClass("grabbable");
              }
            }).on("mousedown.Uploader", function(event){
              event.preventDefault();
              if(event.which === 1 && self.canvas.hasClass("grabbable")){
                self.canvas.addClass("grabbing");
                var grabbedAt = {
                  x: event.offsetX,
                  y: event.offsetY
                };
                self.canvas.on("mousemove.Uploader.move", function(event){
                  var button = self.cropButtonArea.children().filter(function(index, button){
                    return $(button).attr("id") === self.currentVersion.selected;
                  });
                  button.children("i").text(button.attr("icon"));
                  button.addClass("confirm");
                  self.drawCropBox({
                    x: (self.currentVersion.intermediateCrop.x + event.offsetX - grabbedAt.x).clamp(0, self.context.canvas.width - self.currentVersion.intermediateCrop.w),
                    y: (self.currentVersion.intermediateCrop.y + event.offsetY - grabbedAt.y).clamp(0, self.context.canvas.height - self.currentVersion.intermediateCrop.h),
                    w: self.currentVersion.intermediateCrop.w,
                    h: self.currentVersion.intermediateCrop.h
                  });
                });
                self.canvas.one("mouseup.Uploader", function(event, position){
                  self.canvas.removeClass("grabbing");
                  position = Make.defaults(position, {
                    x: event.offsetX,
                    y: event.offsetY
                  });
                  self.currentVersion.intermediateCrop = {
                    x: (self.currentVersion.intermediateCrop.x + position.x - grabbedAt.x).clamp(0, self.context.canvas.width - self.currentVersion.intermediateCrop.w),
                    y: (self.currentVersion.intermediateCrop.y + position.y - grabbedAt.y).clamp(0, self.context.canvas.height - self.currentVersion.intermediateCrop.h),
                    w: self.currentVersion.intermediateCrop.w,
                    h: self.currentVersion.intermediateCrop.h
                  };
                  self.drawCropBox(self.intermediateCrop);
                  self.canvas.off("mousemove.Uploader.move");
                });
              }
            });
          }),
          $("<div />", {
            id: "change-image",
            class: "default",
            text: "Change Image"
          }).data("callback", function(){
            var self = this;

            self.versionFunction = self.replaceVersion;
          })
        ]
      }).data({
        parent: self,
        callback: function(){
          self.resize();
          self.canvas.off().on("mouseout.Uploader", function(event){
            self.canvas.trigger("mouseup.Uploader", {
              x: event.offsetX,
              y: event.offsetY
            });
          }).detach();
          self.backCanvas.off().detach();
          if(self.uploadDialogue){
            self.uploadDialogue.container.hide();
          }
          self.cropButtonArea.hide();
          $(".censor-delete-button").detach();
        }
      });

      self.versionArea = $("<div />", {
        class: "selector"
      }).data({
        parent: self,
        callback: function(element){
          var self = this;

          element = $(element);
          self.currentVersion = self.versions[element.index()];
          self.cropButtonArea.children().each(function(index, button){
            button = $(button);
            if(button.children("i").text() === "crop" || button.children("i").text() === "check"){
              button.remove();
            }
          });
          self.cropButtonArea.prepend(self.currentVersion.crops.keys().map(function(type){
            return $("<div />", {
              id: type,
              icon: self.currentVersion.crops[type]? "check" : "crop",
              label: type
            }).data("callback", function(button){
              var self = this;

              if(type === self.container.attr("crop")){
                self.cropButtonArea.trigger("success.ButtonArea", button);
                self.currentVersion.crops[self.currentVersion.selected] = {
                  x: self.currentVersion.intermediateCrop.x / self.currentVersion.ratios.w,
                  y: self.currentVersion.intermediateCrop.y / self.currentVersion.ratios.h,
                  w: self.currentVersion.intermediateCrop.w / self.currentVersion.ratios.w,
                  h: self.currentVersion.intermediateCrop.h / self.currentVersion.ratios.h
                };
              }else{
                self.cropButtonArea.children(".confirm").removeClass("confirm");
                self.currentVersion.selected = button.attr("id");
                self.container.attr("crop", self.currentVersion.selected);
                button.addClass("confirm");
                self.resetCropCanvas();
                if(self.currentCrop()){
                  self.currentVersion.intermediateCrop = self.currentCrop();
                }else{
                  self.currentVersion.intermediateCrop = self.defaultCropBox();
                }
                self.drawCropBox(self.currentVersion.intermediateCrop);
              }
            });
          }));
          self.image.attr("src", self.currentVersion.image.attr("src"));
        }
      });

      self.cropButtonArea = $("<div />", {
        class: "button-area",
        floating: "true",
        html: [
          $("<div />", {
            html: [
              $("<i />", {
                class: "material-icons",
                text: "close"
              }),
              $("<span />", {
                text: "Clear"
              })
            ]
          }).data("callback", function(){
            var self = this;

            if(!self.currentCrop()) return;
            var cropButton = self.cropButtonArea.children("div").filter(function(index, button){
              return $(button).attr("id") === self.currentVersion.selected;
            }).first();
            cropButton.addClass("confirm");
            cropButton.children("i").text(cropButton.attr("icon"));
            self.currentVersion.crops[self.currentVersion.selected] = undefined;
            self.currentVersion.intermediateCrop = self.defaultCropBox();
            self.drawCropBox(self.currentVersion.intermediateCrop);
          }),
          $("<div />", {
            html: [
              $("<i />", {
                class: "material-icons",
                text: "content_copy"
              }),
              $("<span />", {
                text: "Copy Boxes to Other Versions"
              })
            ]
          }).data("callback", function(){
            var self = this;

            var failed = [];
            self.versions.each(function(index, version){
              if(version !== self.currentVersion){
                var image = version.image.get(0),
                    currentImage = self.currentVersion.image.get(0);
                if(image.naturalWidth === currentImage.naturalWidth && image.naturalHeight === currentImage.naturalHeight){
                  version.crops = self.currentVersion.crops;
                }else{
                  failed.push({
                    index: index,
                    version: version
                  });
                }
              }
            });
            if(failed.length > 0){
              Make.alert(Make.Uploader.options.FAILED_CROP_COPY_MESSAGE(failed));
            }
          })
        ]
      }).data("parent", self);
    },

    resize: function(){
      var self = this;

      var resolution = {
        w: self.image.width(),
        h: self.image.height()
      };
      self.canvas.attr("width", resolution.w);
      self.backCanvas.attr("width", resolution.w);
      self.canvas.attr("height", resolution.h);
      self.backCanvas.attr("height", resolution.h);
      self.currentVersion.ratios = {
        w: resolution.w / self.currentVersion.resolution.w,
        h: resolution.h / self.currentVersion.resolution.h
      }
    },

    loading: function(turnOn){
      var self = this;

      if(turnOn){
        self.container.addClass("loading");
        if(self.versionFunction === self.replaceVersion){
          self.currentVersion.thumbnail.addClass("loading");
        }
      }else{
        self.container.removeClass("loading");
        if(self.currentVersion){
          self.currentVersion.thumbnail.removeClass("loading");
        }
        if(self.files.length > 0){
          self.container.attr("loading", function(index, attr){
            return "(" + (parseInt(/(\d)/.exec(attr)[1]) + 1) + ")";
          });
          self.fileField.trigger("change.Uploader", self.files.splice(0, 1));
        }else if(!self.container.find(".tab-area").exists()){
          self.container.append(self.tabArea);
        }
      }
    },

    load: function(version){
      var self = this;

      self.versionFunction(version);
      self.loading(false);
    },

    firstLoad: function(version){
      var self = this;

      self.container.off("click.Uploader");
      self.container.css({
        cursor: "default"
      });
      version.thumbnail.addClass("default");
      self.container.append(self.image, self.versionArea, self.cropButtonArea.hide());
      $(window).on("resize.Uploader", function(){
        self.resize.call(self);
        self.tabArea.children(".selected").trigger("click.TabArea");
      });
      self.versionFunction = self.newVersion;
      self.versionFunction(version);
    },

    newVersion: function(version){
      var self = this;

      self.versions.push(version);
      self.versionArea.append(version.thumbnail);
      self.tabArea.children(".selected").trigger("click.TabArea");
    },

    replaceVersion: function(version){
      var self = this;

      self.versions[self.versionArea.children(".selected").index()] = version;
      self.versionArea.children(".selected").replaceWith(version.thumbnail);
      version.thumbnail.trigger("click.Selector");
    },

    resetCropCanvas: function(){
      var self = this;

      self.context.globalCompositeOperation = "source-over";
      self.context.clearRect(0, 0, self.context.canvas.width, self.context.canvas.height);
      self.context.fillStyle = "rgba(0, 0, 0, 0.8)";
      self.context.fillRect(0, 0, self.context.canvas.width, self.context.canvas.height);
      self.context.globalCompositeOperation = "destination-out";
    },

    defaultCropBox: function(){
      var self = this;

      if((self.context.canvas.width < self.context.canvas.height && self.context.canvas.height * self.currentVersion.ratio() > self.context.canvas.width) || (self.context.canvas.width > self.context.canvas.height && self.context.canvas.width * self.currentVersion.ratio() <= self.context.canvas.height)){
        return {
          x: 0,
          y: 0,
          w: self.context.canvas.width,
          h: self.context.canvas.width * self.currentVersion.ratio()
        };
      }else{
        return {
          x: 0,
          y: 0,
          w: self.context.canvas.height * self.currentVersion.ratio(),
          h: self.context.canvas.height
        };
      }
    },

    drawCropBox: function(properties){
      var self = this;

      properties = Make.default(properties, self.currentVersion.intermediateCrop);
      self.resetCropCanvas();
      self.context.fillRect(properties.x, properties.y, properties.w, properties.h);
    },

    currentCrop: function(){
      if(!this.currentVersion.selected || !this.currentVersion.crops[this.currentVersion.selected]) return undefined;
      return {
        x: this.currentVersion.crops[this.currentVersion.selected].x * this.currentVersion.ratios.w,
        y: this.currentVersion.crops[this.currentVersion.selected].y * this.currentVersion.ratios.h,
        w: this.currentVersion.crops[this.currentVersion.selected].w * this.currentVersion.ratios.w,
        h: this.currentVersion.crops[this.currentVersion.selected].h * this.currentVersion.ratios.h
      };
    },

    resetCensorCanvas: function(){
      var self = this;

      self.context.mozImageSmoothingEnabled = false;
      self.context.webkitImageSmoothingEnabled = false;
      self.context.imageSmoothingEnabled = false;
      self.context.globalCompositeOperation = "source-over";
      self.backContext.clearRect(0, 0, self.backContext.canvas.width, self.backContext.canvas.height);
      self.context.clearRect(0, 0, self.context.canvas.width, self.context.canvas.height);
    },

    drawCurrentCensors: function(){
      var self = this;

      self.currentVersion.censors.each(function(index, box){
        self.drawCensorBox({
          x: box.x * self.currentVersion.ratios.w,
          y: box.y * self.currentVersion.ratios.h,
          w: box.w * self.currentVersion.ratios.w,
          h: box.h * self.currentVersion.ratios.h
        });
      });
    },

    drawCensorBox: function(properties){
      var self = this;

      self.backContext.drawImage(self.image.get(0), properties.x / self.currentVersion.ratios.w, properties.y / self.currentVersion.ratios.h, Math.max(1, properties.w / self.currentVersion.ratios.w), Math.max(1, properties.h / self.currentVersion.ratios.h), 0, 0, properties.w * Make.Uploader.options.PIXELATION_FACTOR, properties.h * Make.Uploader.options.PIXELATION_FACTOR);
      self.context.drawImage(self.backCanvas.get(0), 0, 0, Math.max(1, properties.w * Make.Uploader.options.PIXELATION_FACTOR), Math.max(1, properties.h * Make.Uploader.options.PIXELATION_FACTOR), properties.x, properties.y, properties.w, properties.h);
    }

  });

  return Uploader;

})();