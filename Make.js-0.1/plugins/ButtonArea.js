Make.ButtonArea.object = (function(){

  var ButtonArea = new Function();

  Make.extension(ButtonArea.prototype, {

    make: function(){
      var self = this;
      Make.public.call(self, "success");

      self.parent = self.container.data("parent");
      self.buttons = self.container.children("div");
      self.buttons.each(function(index, button){
        self.connect(button);
      });
      if(MutationObserver){
        self.observer = new MutationObserver(function(event){
          event.each(function(index, record){
            Array.from(record.addedNodes).each(function(index, node){
              self.connect(node);
            })
          })
        });
        self.observer.observe(self.container.get(0), {
          childList: true
        });
      }else{
        self.container.on("DOMNodeInserted.Make", function(event){
          self.connect(event.target);
        });
      }
    },

    connect: function(button){
      var self = this;

      button = $(button);
      if(button.attr("icon") && !button.children("i").exists()){
        button.append($("<i />", {
          class: "material-icons",
          text: button.attr("icon")
        }));
      }
      if(button.attr("label") && !button.children("span").exists()){
        button.append($("<span />", {
          text: button.attr("label")
        }));
      }
      var up = function(){
        button.removeClass("depressed");
        var func, executed = false;
        switch(true){
          case(typeof self.parent === "object"):
            func = button.data("callback");
            if(typeof func === "function"){
              func.call(self.parent, button);
              executed = true;
            }
          case(button.is("[id]")):
            if(Make.ButtonArea.functions){
              func = Make.ButtonArea.functions["#" + button.attr("id")];
              if(typeof func === "function"){
                func.call(button);
                executed = true;
              }
            }
          case(button.is("[class]")):
            if(Make.ButtonArea.functions){
              func = button.attr("class").split(/\s/);
              func.each(function(index, func){
                func = Make.ButtonArea.functions["." + func];
                if(typeof func === "function"){
                  func.call(button);
                  executed = true;
                }
              });
            }
            if(executed) break;
          default:
            console.log("No associated function.");
        }
      }, down = function(){
        button.addClass("depressed");
      };
      button.on("mousedown.Notification", function(event){
        if(event.which === 1){
          down();
        }
      });
      button.on("mouseup.Notification", function(event){
        if(event.which === 1){
          up();
        }
      });
      if(typeof button.data("keys") === "string"){
        button.data("keys").split(",").each(function(index, key){
          key = key.trim();
          $(window).on("keydown.Notification", function(event){
            if(event.keyCode === Make.keyCode(key) && button.is(":visible")){
              down();
            }
          });
          $(window).on("keyup.Notification", function(event){
            if(event.keyCode === Make.keyCode(key) && button.is(":visible")){
              up();
            }
          });
        });
      }
    },

    success: function(button){
      button = $(button);
      button.off("animationend").on("animationend", function(){
        button.removeClass("success");
      });
      button.removeClass("confirm").addClass("success");
      button.children("i").text("check");
    }

  });

  return ButtonArea;

})();