if(!Make.dialogueBox){
  Make.dialogueBox = function(pages){
    var object = Make.object("DialogueBox");
    object.pages = Make.default(pages, []);
    object.render();
    return object;
  };
}

Make.DialogueBox.object = (function(){

  var DialogueBox = function(){
    Make.plugin("ButtonArea");
  };

  Make.extension(DialogueBox.prototype, {

    make: function(){
      var self = this;

      self.container = $("<div />", {
        id: "dialogue-box",
        html: [
          $("<div />", {
            id: "content",
            html: [
              (self.contentArea = $("<div />", {
                id: "content-area"
              }))
            ]
          })
        ]
      }).appendTo($(document.body));
      (self.buttonArea = $("<div />", {
        class: "button-area"
      })).data("parent", self).insertAfter(self.contentArea);
      self.pages = [{}];
      self.currentPage = 0;
      self.render();
      Make.public.call(self, "prev", "next");
    },

    render: function(properties){
      var self = this;

      if(!properties){
        self.render(self.pages.first());
        return;
      }
      self.contentArea.html(Make.default(properties.content, ""));
      properties.buttons = Make.default(properties.buttons, []);
      self.buttonArea.html("");
      properties.buttons.each(function(name, properties){
        $("<div />", {
          id: name.dasherize(),
          label: name
        }).data(properties).appendTo(self.buttonArea);
      });
    },

    prev: function(){
      this.currentPage = Math.max(0, this.currentPage - 1);
      this.render(this.pages[this.currentPage]);
    },

    next: function(){
      this.currentPage = Math.min(this.currentPage + 1, this.pages.length - 1);
      this.render(this.pages[this.currentPage]);
    }

  });

  return DialogueBox;

})();