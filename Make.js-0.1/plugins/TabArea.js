Make.TabArea.object = (function(){

  var TabArea = new Function();

  Make.extension(TabArea.prototype, {

    make: function(){
      var self = this;
      Make.public.call(self, "prev", "next");

      self.tabs = {};
      self.history = [];
      self.selected = 0;
      self.container.children().each(function(index, element){
        self.connect(element);
      });
      self.select(self.container.children(".default"));
      if(MutationObserver){
        self.observer = new MutationObserver(function(event){
          event = event.first();
          self.connect(event.addedNodes[0]);
          if(event.removedNodes[0] && $(event.removedNodes[0]).is(".selected")){
            self.select(event.addedNodes[0]);
          }
        });
        self.observer.observe(self.container.get(0), {
          childList: true
        });
      }else{
        self.container.on("DOMNodeInserted.Make", function(event){
          self.connect(event.target);
        });
      }
    },

    connect: function(element){
      var self = this;

      element = $(element);
      element.off("click.TabArea").on("click.TabArea", function(){
        element = $(element);
        self.select.call(self, element);
      });
    },

    select: function(element){
      var self = this;

      element = $(element);
      if(typeof self.container.data("parent") === "object" && typeof self.container.data("callback") === "function"){
        self.container.data("callback").call(self.container.data("parent"));
      }
      if(!element.is("[track = false]")){
        self.container.children(".selected").removeClass("selected");
        element.addClass("selected");
        self.container.data("parent").container.attr("tab", element.attr("id"));
        self.history.push(element);
        self.selected = self.history.length - 1;
      }
      if(typeof self.container.data("parent") === "object" && typeof element.data("callback") === "function"){
        element.data("callback").call(self.container.data("parent"));
      }
    },

    prev: function(){
      var self = this;

      self.selected = Math.max(self.selected - 1, 0);
      self.select(self.history[self.selected]);
    },

    next: function(){
      var self = this;

      self.selected = Math.min(self.selected + 1, self.history.length - 1);
      self.select(self.history[self.selected]);
    }

  });

  return TabArea;

})();