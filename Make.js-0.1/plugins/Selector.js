Make.Selector.object = (function(){

  var Selector = new Function();

  Make.extension(Selector.prototype, {

    make: function(){
      var self = this;

      self.parent = self.container.data("parent");
      self.callback = self.container.data("callback");
      self.container.children().each(function(index, element){
        self.connect(element);
      });
      self.select(self.container.children(".default"));
      if(MutationObserver){
        self.observer = new MutationObserver(function(event){
          event = event.first();
          if(event.addedNodes[0]){
            self.connect(event.addedNodes[0]);
            if(event.removedNodes[0] && $(event.removedNodes[0]).is(".selected")){
              self.select(event.addedNodes[0]);
            }
          }
        });
        self.observer.observe(self.container.get(0), {
          childList: true
        });
      }else{
        self.container.on("DOMNodeInserted.Make", function(event){
          self.connect(event.target);
        });
      }
    },

    connect: function(element){
      var self = this;

      element = $(element);
      element.on("click.Selector", function(){
        self.select.call(self, element);
      });
    },

    select: function(element){
      var self = this;

      element = $(element);
      self.container.children(".selected").removeClass("selected");
      element.addClass("selected");
      if(typeof self.parent === "object" && typeof self.callback === "function"){
        self.callback.call(self.parent, element);
      }
      return element;
    }

  });

  return Selector;

})();