Expandable = (function(){

  var Expandable = function(element){
    var self = this;

    self.container = $(element).on("resize.Expandable", function(event, heightDifference){
      Make.pause(event);
      self.resize.call(self, heightDifference);
    });
    self.title = $("<div />", {
      id: "expandable-title",
      text: self.container.attr("title")
    }).on("click.Expandable", function(){
      self.flip.call(self);
    });

    $(document).on("ready.Expandable", function(){
      if(self.container.is("[title]")) self.container.prepend(self.title);
      self.container.removeAttr("up");
      if(self.container.is("[down]")){
        self.container.css("height", self.container.prop("scrollHeight") - self.container.prop("clientTop"));
      }else{
        self.container.attr("up", "");
        self.container.css("height", self.title.prop("scrollHeight"));
      }
    });
  };

  Make.extension(Expandable.prototype, {

    resize: function(heightDifference){
      var self = this;

      setTimeout(function(){
        if(self.container.is("[down]")){
          console.log(self.container.get(0), heightDifference);
          self.container.css("height", self.container.prop("scrollHeight") + heightDifference);
          self.container.parents("expandable").first().trigger("resize.Expandable", heightDifference);
        }
      }, 50);
    },

    flip: function(){
      var self = this;

      if(!self.container.is("[animate]")) self.container.attr("animate", "");
      if(self.container.is("[up]")){
        self.container.removeAttr("up").attr("down", "");
        var containerHeight = self.container.prop("scrollHeight") - self.container.prop("clientTop"),
            titleHeight = self.title.prop("scrollHeight");
        self.container.css("height", containerHeight).parents("expandable").trigger("resize.Expandable", containerHeight - titleHeightX);
      }else{
        self.container.removeAttr("down").attr("up", "");
        var containerHeight = self.container.prop("scrollHeight") - self.container.prop("clientTop"),
            titleHeight = self.title.prop("scrollHeight");
        self.container.css("height", titleHeight).parents("expandable").first().trigger("resize.Expandable", titleHeight - containerHeight);
      }
      
    },

  });

  return Expandable;

})();