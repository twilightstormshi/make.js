Make.Chooser.object = (function(){

  var Chooser = new Function();

  Make.extension(Chooser.prototype, {

    make: function(){
      var self = this;
      Make.public.call(self, "accept", "revert");

      if(!self.container.children().exists()){
        self.container.append([
          (self.labelArea = $("<div />", {
            class: "label-area",
            html: $("<span />", {
              text: self.container.attr("label")
            })
          })),
          (self.valueArea = $("<div />", {
            class: "value-area"
          }))
        ]);
        if(typeof self[self.container.attr("type")] === "function"){
          self[self.container.attr("type")]();
        }
      }
    },

    file: function(){
      var self = this;

      self.fileInput = $("<input />", {
        type: "file"
      }).on("change.Chooser", function(event){
        self.fileDisplayArea.html([
          $("<span />", {
            text: self.fileInput.prop("value")
          }),
          $("<i />", {
            class: "material-icons",
            text: "close"
          }).on("click.Chooser", function(event){
            Make.pause(event);
            self.fileInput.prop("value", "");
            self.fileDisplayArea.html("");
          })
        ])
      }).appendTo(self.valueArea).hide();
      self.fileDisplayArea = $("<div />", {
        class: "chooser-file"
      }).on("click.Chooser", function(){
        self.fileInput.show();
        self.fileInput.trigger("click.Chooser");
        self.fileInput.hide();
      }).appendTo(self.valueArea);
      self.accept = function(){};
    },

    textbox: function(){
      var self = this;

      self.valueArea.append($("<input />", {
        type: "text"
      }).on("change.Chooser", function(){
        self.container.attr("value", this.value);
      }));
      self.revert = function(){
        self.container.attr("value", Make.default(self.container.data("prevValue"), ""));
        self.valueArea.children().prop("value", self.container.attr("value"));
      };
    },

    radio: function(){
      var self = this;

      if(!self.container.attr("choices")) return;
      self.container.attr("choices").split(",").each(function(index, choice){
        choice = choice.trim();
        self.valueArea.append($("<div />", {
          id: choice,
          text: choice
        }));
      });
      self.valueArea.children().each(function(index, choice){
        choice = $(choice);
        choice.on("click.Chooser", function(){
          self.valueArea.children().removeClass("selected");
          choice.addClass("selected");
          self.container.attr("value", choice.text());
        });
      });
      self.revert = function(){
        self.container.attr("value", Make.default(self.container.data("prevValue"), ""));
        self.valueArea.children(".selected").removeClass("selected");
        self.valueArea.children("[id = '"+ self.container.attr("value") + "']").trigger("click.Chooser");
      };
    },

    checkbox: function(){
      var self = this;

      if(!self.container.attr("choices")) return;
      self.container.attr("choices").split(",").each(function(index, choice){
        choice = choice.trim();
        self.valueArea.append($("<div />", {
          id: choice,
          text: choice
        }));
      });
      var update = function(){
        self.container.attr("value", self.valueArea.children(".selected").get().map(function(choice){
          return $(choice).text();
        }).join(", "));
      };
      var select = function(choice){
        choice.one("click.Chooser", function(){
          choice.addClass("selected");
          update();
          choice.one("click.Chooser", function(){
            choice.removeClass("selected");
            update();
            select(choice);
          });
        });
      }
      self.valueArea.children().each(function(index, choice){
        select($(choice));
      });
      self.revert = function(){
        self.container.attr("value", Make.default(self.container.data("prevValue"), ""));
        self.valueArea.children().each(function(index, choice){
          choice = $(choice);
          choice.removeClass("selected");
          choice.off();
          select(choice);
          if(self.container.attr("value").split(", ").contains(choice.attr("id"))){
            choice.trigger("click.Chooser");
          }
        });
      };
    },

    accept: function(){
      this.container.data("prevValue", this.container.attr("value"));
    },

    revert: function(){}

  });

  return Chooser;

})();