Make = {};

Object.defineProperty(Object.prototype, "isDictionary", {
  value: function(){
    if(!this || Array.isArray(this) || this.constructor != Object) return false;
    return true;
  },
  writable: true,
  configurable: true,
  enumerable: false
});

Make.extension = function(obj, prop, func){
  if(prop.isDictionary()){
    $.each(prop, function(key, value){
      Make.extension(obj, key, value);
    });
  }else{
    Object.defineProperty(obj, prop, {
      value: func,
      writable: true,
      configurable: true,
      enumerable: false
    });
  }
};

Make.loaderOverlay = $("<div />", {
  id: "MakeJS-loader",
  css: {
    position: "fixed",
    top: 0,
    left: 0,
    height: "100vh",
    width: "100vw",
    backgroundColor: "rgba(112,128,144,0.8)",
    color: "ghostwhite",
    textAlign: "center",
    "lineHeight": "90vh",
    "fontSize": "5vw"
  },
  text: "Loading..."
}).appendTo($(document.body));

Make.extension(Make, {

  plugin: function(plugin, configs){
    if(Make[plugin]) return;
    Make.loaderOverlay.show();
    Make[plugin] = {};
    var promises = [];
    Make.plugins.plugins = Make.default(Make.plugins.plugins, {});
    configs = Make.default(configs, []);
    Make.plugins.plugins[plugin] = configs;
    configs.each(function(index, config){
      promises.push($.ajax({
        url: Make.fileRoot + "/" + Make.plugins.configPath + "/" + plugin + "." + config,
        crossDomain: true,
        dataType: "script",
        success: function(data){
          eval(data);
        }
      }));
    });
    promises.push($("<link />", { rel: "stylesheet", href: Make.fileRoot + "/" + Make.plugins.pluginPath + "/" + plugin + ".css" }).appendTo($("head")));
    promises.push($.ajax({
      url: Make.fileRoot + "/" + Make.plugins.pluginPath + "/" + plugin + ".js",
      crossDomain: true,
      dataType: "script",
      success: function(data){
        eval(data);
      }
    }))
    $.when.apply($, promises).done(function(){
      Make[plugin].objects = [];
      $("." + plugin.dasherize()).get().reverse().each(function(index, element){
        Make.object(plugin, element);
      });
      Make.loaderOverlay.hide();
    });
  },

  object: function(plugin, container){
    var object = new Make[plugin].object();
    Make[plugin].objects.push(object);
    object.container = $(container);
    object.make();
    object.container.data("object", object);
    return object;
  },

  public: function(){
    var self = this;

    arguments.each(function(index, func){
      if(typeof self[func] === "function"){
        var klass = Make.default(self.container.attr("class"), self.container.attr("id"));
        if(!klass) return;
        klass = klass.split(" ").filter(function(klass){
          return Make.plugins.plugins.keys().contains(klass.camelCase().capitalize(true));
        }).first().camelCase().capitalize(true);
        self.container.off(func + "." + klass).on(func + "." + klass, function(){
          if(typeof self[func] === "function"){
            self[func].apply(self, arguments.values().slice(1));
          }
        });
      }
    })
  },

  default: function(prop, defaultValue, acceptedValues){
    if(acceptedValues === undefined){
      return prop === undefined || (acceptedValues && !acceptedValues.contains(prop))? defaultValue : prop;
    }
  },

  defaults: function(original, defaults){
    original = Make.default(original, {});
    defaults.keys().each(function(index, key){
      original[key] = Make.default(original[key], defaults[key]);
    });
    return original;
  },

  pause: function(event){
    if(event.stopPropagation) event.stopPropagation();
    if(event.preventDefault) event.preventDefault();
    event.cancelBubble = true;
    event.returnValue = false;
  },

  keyCodes: {
    "backspace": 8,
    "enter": 13,
    "esc": 27,
    "left": 37,
    "right": 39
  },

  keyCode: function(key){
    return Make.keyCodes[key];
  }

});

Make.fileRoot = /(.*\/).+$/.test($("script").filter(function(index, script){
  return /make.js/.test(script.src);
}).attr("src"))? RegExp.$1 : "";

$(document).on("ready.Make", function(){
  $.ajax({
    url: Make.fileRoot + "make.plugins",
    dataType: "json",
    success: function(data){
      Make.plugins = data;
      Make.plugins.autoload.each(function(plugin, configs){
        if($("." + plugin.dasherize()).exists()){
          Make.plugin(plugin, configs);
        }
      });
      var load = function(element){
        element = $(element);
        Make.plugins.plugins.each(function(plugin, configs){
          if(element.hasClass(plugin.dasherize())){
            Make.object(plugin, element);
          }
        });
      };
      if(MutationObserver){
        Make.observer = new MutationObserver(function(event){
          event.each(function(index, record){
            Array.from(record.addedNodes).reverse().each(function(index, element){
              load(element);
            });
          });
        });
        Make.observer.observe(document.body, {
          childList: true,
          subtree: true
        });
      }else{
        $(document).on("DOMNodeInserted.Make", function(event){
          load(event.target);
        });
      }
    }
  });
});


Make.extension(Number.prototype, {

  clamp: function(min, max){
    return Math.max(min, Math.min(this, max));
  },

  asIndexIn: function(array){
    return this.clamp(0, array.length - 1);
  }

});

Make.extension(Array.prototype, {

  each: function(func){
    return $.each(this, func);
  },

  first: function(element){
    if(element === undefined){
      return this[0];
    }else{
      this[0] = element;
      return this;
    }
  },

  last: function(element){
    if(element === undefined){
      return this[this.length - 1];
    }else{
      this[this.length - 1] = element;
      return this;
    }
  },

  contains: function(element){
    return this.indexOf(element) > -1;
  },

  humanize: function(joiningWord){
    joiningWord = Make.default(joiningWord, "and");
    return this.toString().replace(/,/g, ", ").replace(/, (?=\S+$)/, " " + joiningWord + " ");
  },

  empty: function(){
    this.length = 0;
    return this;
  },

  isEmpty: function(){
    return this.length === 0;
  },

  removeAt: function(index){
    this.splice(index, 1);
    return this;
  },

  remove: function(element){
    if(this.contains(element)){
      this.removeAt(this.indexOf(element));
    }
    return this;
  }

});

Make.extension(String.prototype, {

  capitalize: function(majuscule = true){
    if(majuscule) return this.slice(0, 1).toUpperCase() + this.slice(1);
    return this.slice(0, 1).toLowerCase() + this.slice(1);
  },

  camelCase: function(){
    return (this.split(/_|\s|-/).map(function(word, index){
      if(index === 0) return word.capitalize(false);
      return word.capitalize();
    })).join("");
  },

  dasherize: function(){
    var split = this.split(/_|\s/);
    if(split.length === 1) return (this.capitalize(false)).replace(/([A-Z])/g, '-$1').toLowerCase();
    return split.join("-").toLowerCase();
  }

});

Make.extension(Object.prototype, {

  keys: function(){
    if(!this.isDictionary()) return this;
    return Object.keys(this);
  },

  values: function(){
    if(!this.isDictionary()) return this;
    var self = this;
    return this.keys().map(function(key){
      return self[key];
    });
  },

  each: function(func){
    if(!this.isDictionary()) return this;
    return $.each(this, func);
  }

});

$(function($){

  $.fn.extend({

    exists: function(){
      if(this.length < 1){
        return false;
      };
      return this;
    },

    insert: function(item, index){
      if(index === 0){
        this.prepend(item);
      }else if(index === this.children().length){
        this.append(item);
      }else{
        this.children().eq(index).before(item);
      }
      return this;
    },

    object: function(){
      if(this.data("object")){
        return this.data("object");
      }else{
        console.error("No object reference yet. .object() only works in user-triggered events.")
      }
    }

  });

});