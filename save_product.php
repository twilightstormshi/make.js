<?php
  $version = $_POST["version"];
  error_log($version);
  $version = json_decode($version);
  $title = $_POST["title"];
  error_log($title);
  $image = base64_decode($version->{"image"});
  $im = new Imagick();
  $im->readImageBlob($image);
  header("Content-type: ".$im->getImageMimeType());
  file_put_contents("images/".$title." - FULL.".$version->{"filetype"}, $im->getImageBlob());
  foreach($version->{"crops"} as $name => $crop){
    $im->cropImage($crop->{"w"}, $crop->{"h"}, $crop->{"x"}, $crop->{"y"});
    foreach($version->{"censors"} as $censor){
      $blackbox = new Imagick();
      $blackbox->newImage($censor->{"w"}, $censor->{"h"}, "black", "png");
      $im->compositeImage($blackbox, Imagick::COMPOSITE_ATOP, $censor->{"x"}, $censor->{"y"});
    }
    file_put_contents("images/".$title." - ".$version->{"name"}." - ".$name.".".$version->{"filetype"}, $im->getImageBlob());
    $im->readImageBlob($image);
  }
?>