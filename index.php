<html>
<head>
  <script src = "/Make.js-0.1/jquery.js"></script>
  <script src = "/Make.js-0.1/make.js"></script>
</head>
<body>
  <?php
  function formattedSizeToBytes($sSize){
    if(is_numeric($sSize)){
      return $sSize;
    }
    $sSuffix = substr($sSize, -1);
    $iValue = substr($sSize, 0, -1);
    switch(strtoupper($sSuffix)){
    case 'P':
      $iValue *= 1024;
    case 'T':
      $iValue *= 1024;
    case 'G':
      $iValue *= 1024;
    case 'M':
      $iValue *= 1024;
    case 'K':
      $iValue *= 1024;
      break;
    }
    return $iValue;
  }
  ?>
  <div class = "uploader" post-url = "save_product.php" redirect = "redirect.php" max-upload-size = "<?= formattedSizeToBytes(ini_get('upload_max_filesize')); ?>" ></div>
</body>
</html>